## Drone delivery.

By: *Joshua Puello Padilla*

### Stack:
- Maven
- Java 8
- Junit 4
- Log4j2

### Execution:
- Clone this repository.
- Import on your IDE as a Maven project.
- Run the main class ```App.java```
- You can see the generated files in ```src/main/resources/output/```
---
 
### Additional information
There is an special test case called ```whenPassingValidListOfRoutes_thenGetListOfPositions``` that shows the result of
the given scenario where the input is:

```
AAAAIAA
DDDAIAD
AAIADAD
```
Following the given business logic, the result differs from the indicated in the example.

Expected:
```
(-2, 4) dirección Norte
(-3, 3) dirección Sur
(-4, 2) dirección Oriente
```
The test responses with the following:
```
(-2, 4) dirección Oeste
(-1, 3) dirección Sur
(0, 0) dirección Oeste
```