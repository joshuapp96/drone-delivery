package com.java.s4n;

import com.java.s4n.business.strategy.DeliverBehavior;
import com.java.s4n.business.strategy.DroneDeliver;
import com.java.s4n.domain.DirectionEnum;
import com.java.s4n.domain.Position;
import com.java.s4n.domain.Route;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.java.s4n.domain.DirectionEnum.OESTE;
import static com.java.s4n.domain.DirectionEnum.SUR;
import static org.junit.Assert.assertEquals;

public class DroneDeliverTest {

    private DeliverBehavior deliver;
    private Position initialPosition;

    @Before
    public void init() {
        this.deliver = new DroneDeliver();
        this.initialPosition = Position.initial();
    }

    @Test
    public void whenPassingValidListOfRoutes_thenGetListOfPositions() {

        List<Route> routes = new ArrayList<>();
        List<Position> expectedPositions = new ArrayList<>();
        Integer deliveryCoverage = 10;
        
        routes.add(new Route("AAAAIAA"));
        routes.add(new Route("DDDAIAD"));
        routes.add(new Route("AAIADAD"));
        
        expectedPositions.add(new Position(-2, 4, OESTE));
        expectedPositions.add(new Position(-1, 3, SUR));
        expectedPositions.add(new Position(0, 0, OESTE));

        List<Position> positionsList = routes.stream()
                .map(route -> this.deliver.deliver(deliveryCoverage, this.initialPosition, route))
                .collect(Collectors.toList());

        assertEquals(positionsList.toString(), expectedPositions.toString());
    }

    @Test
    public void whenPassingValidRoute_thenGetFinalPosition() {

        Route route = new Route("AAADA");
        Integer deliveryCoverage = 10;
        String expectedPosition = "(1, 3) dirección Este";

        Position finalPosition = this.deliver.deliver(deliveryCoverage, this.initialPosition, route);

        assertEquals(expectedPosition, finalPosition.toString());
    }

    @Test
    public void whenPassingInvalidRouteOutOfCoverageLimit_thenGetLastValidPosition() {

        Route route = new Route("AAADIADAAAAAA");
        Integer deliveryCoverage = 4;
        String expectedPosition = "(4, 4) dirección Este";

        Position finalPosition = this.deliver.deliver(deliveryCoverage, this.initialPosition, route);

        assertEquals("With a larger limit the result would be (6, 4) dirección Este",
                expectedPosition, finalPosition.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenPassingInvalidStepInRoute_thenThrowException() {

        Route route = new Route("AAADIB");
        Integer deliveryCoverage = 4;

        this.deliver.deliver(deliveryCoverage, this.initialPosition, route);
    }
}
