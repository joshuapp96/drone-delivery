package com.java.s4n;

import com.java.s4n.business.observable.DeliveryStation;
import com.java.s4n.business.observer.FileExporter;
import com.java.s4n.business.observer.Observer;
import com.java.s4n.domain.request.DeliveryRequestModel;

public class App {
    public static void main(String[] args) {

        DeliveryRequestModel model = new DeliveryRequestModel.Builder().build();
        Observer observer = new FileExporter(model);

        DeliveryStation deliveryStation = new DeliveryStation(model);
        deliveryStation.register(observer);

        deliveryStation.deliverOrders();
    }
}