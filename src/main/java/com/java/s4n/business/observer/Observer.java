package com.java.s4n.business.observer;

import com.java.s4n.domain.Position;

import java.util.List;

public interface Observer {

    void update(String conveyorName, List<Position> deliveryResult);

}
