package com.java.s4n.business.observer;

import com.java.s4n.domain.Position;
import com.java.s4n.domain.request.DeliveryRequestModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileExporter implements Observer {

    private static final Logger LOGGER = LogManager.getLogger(FileExporter.class);

    private DeliveryRequestModel model;

    public FileExporter(DeliveryRequestModel model) {
        this.model = model;
    }

    @Override
    public void update(String conveyorName, List<Position> deliveryResult) {
        try {
            Path pathToFile = Paths.get(model.getOutputPath(),
                    model.getOutputFilePrefix() + conveyorName + model.getFileExtension());
            List<String> lines = new ArrayList<>();
            lines.add(model.getHeader());
            lines.addAll(deliveryResult.stream().map(Position::toString).collect(Collectors.toList()));
            Files.write(pathToFile, lines, Charset.defaultCharset());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
