package com.java.s4n.business.strategy;

import com.java.s4n.domain.DirectionEnum;
import com.java.s4n.domain.MovementEnum;
import com.java.s4n.domain.Position;
import com.java.s4n.domain.Route;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DroneDeliver implements DeliverBehavior {

    private static final Logger LOGGER = LogManager.getLogger(DroneDeliver.class);
    
    @Override
    public Position deliver(Integer deliveryCoverage, Position currentPosition, Route route) {
        String[] steps = route.getCoordinates().split("");
        DirectionEnum currentDirection = currentPosition.getDirection();

        for (String step : steps) {
            MovementEnum movement = getMovementFromStep(step);
            switch (movement) {
                case A:
                    if (!performMovement(currentDirection, currentPosition, deliveryCoverage)) {
                        return Position.from(currentPosition);
                    }
                    break;
                case D:
                    currentDirection = DirectionEnum.valueOf(currentDirection.name()).toRight();
                    currentPosition.setDirection(currentDirection);
                    break;
                case I:
                    currentDirection = DirectionEnum.valueOf(currentDirection.name()).toLeft();
                    currentPosition.setDirection(currentDirection);
                    break;
                default:
                    break;
            }
        }
        return Position.from(currentPosition);
    }
    
    private MovementEnum getMovementFromStep(String step) {
        MovementEnum movement = null;
        try {
            movement = MovementEnum.valueOf(step);
        } catch (IllegalArgumentException e) {
            LOGGER.error(String.format("No valid step [%s] in the given coordinate.", step));
            throw e;
        }
        return movement;
    }

    private boolean performMovement(DirectionEnum currentDirection, Position currentPosition, Integer deliverCoverage) {
        switch (currentDirection) {
            case NORTE:
                if (currentPosition.getY() == deliverCoverage) return false;
                currentPosition.incrementYAxis();
                break;
            case SUR:
                if (currentPosition.getY() == -deliverCoverage) return false;
                currentPosition.decrementYAxis();
                break;
            case ESTE:
                if (currentPosition.getX() == deliverCoverage) return false;
                currentPosition.incrementXAxis();
                break;
            case OESTE:
                if (currentPosition.getX() == -deliverCoverage) return false;
                currentPosition.decrementXAxis();
                break;
            default:
                break;
        }
        return true;
    }
}
