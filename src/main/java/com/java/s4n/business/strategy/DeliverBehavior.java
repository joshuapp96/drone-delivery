package com.java.s4n.business.strategy;

import com.java.s4n.domain.Position;
import com.java.s4n.domain.Route;

public interface DeliverBehavior {

    Position deliver(Integer deliverCoverage, Position currentPosition, Route route);

}
