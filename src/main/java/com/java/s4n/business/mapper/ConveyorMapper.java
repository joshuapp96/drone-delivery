package com.java.s4n.business.mapper;

import com.java.s4n.domain.Route;
import com.java.s4n.domain.factory.Conveyor;
import com.java.s4n.domain.factory.ConveyorBuilderFactory;
import com.java.s4n.domain.request.DeliveryRequestModel;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ConveyorMapper {

    private ConveyorBuilderFactory conveyorBuilderFactory;
    private DeliveryRequestModel deliveryRequestModel;

    public ConveyorMapper(DeliveryRequestModel deliveryRequestModel) {
        this.deliveryRequestModel = deliveryRequestModel;
        this.conveyorBuilderFactory = new ConveyorBuilderFactory();
    }

    private Conveyor getMappedConveyor(String fileName, List<String> lines) {
        Conveyor conveyor = null;
        Conveyor.Builder conveyorBuilder = conveyorBuilderFactory.createConveyorBuilder(deliveryRequestModel.getConveyorType());
        conveyor = conveyorBuilder
                .withName(getConveyorIdxFromFileName(fileName))
                .withRoutes(lines.stream().map(Route::new).collect(Collectors.toList()))
                .withCapacity(deliveryRequestModel.getConveyorCapacity())
                .withDeliveryCoverage(deliveryRequestModel.getDeliverCoverage())
                .build();
        return conveyor;
    }

    private String getConveyorIdxFromFileName(String fileName) {
        return fileName.replaceAll("\\D+", "");
    }

    public List<Conveyor> getConveyors(Map<String, List<String>> coordinatesByConveyorName) {
        return coordinatesByConveyorName
                .entrySet().stream()
                .map(e -> getMappedConveyor(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

}