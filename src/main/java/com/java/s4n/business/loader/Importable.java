package com.java.s4n.business.loader;

import com.java.s4n.domain.request.DeliveryRequestModel;

import java.util.List;
import java.util.Map;

public abstract class Importable {

    protected DeliveryRequestModel model;

    public Importable(DeliveryRequestModel deliveryRequestModel) {
        this.model = deliveryRequestModel;
    }

    public abstract Map<String, List<String>> importCoordinatesByConveyorName();
}
