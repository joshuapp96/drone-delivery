package com.java.s4n.business.loader;

import com.java.s4n.domain.request.DeliveryRequestModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileImporter extends Importable {

    private static final Logger LOGGER = LogManager.getLogger(FileImporter.class);

    public FileImporter(DeliveryRequestModel model) {
        super(model);
    }

    private List<Path> getPathsFromLocation() {

        List<Path> paths = new ArrayList<>();
        String fileNamePattern = model.getInputFilePrefix() + "[\\d]+" + model.getFileExtension();
        Pattern pattern = Pattern.compile(fileNamePattern);

        try (Stream<Path> walk = Files.walk(Paths.get(model.getInputPath()))) {
            paths = walk
                    .filter(path -> Files.isRegularFile(path) &&
                            pattern.matcher(path.getFileName().toString()).matches())
                    .limit(model.getConveyorFleetSize())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        return paths;
    }

    private List<String> readLines(Path path) {
        List<String> linesByFile = new ArrayList<>();
        try (Stream<String> lines = Files.lines(path);) {
            linesByFile = lines
                    .limit(model.getConveyorCapacity())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        return linesByFile;
    }

    @Override
    public Map<String, List<String>> importCoordinatesByConveyorName() {
        List<Path> paths = getPathsFromLocation();
        return paths.stream().collect(Collectors.toMap(
                path -> path.getFileName().toString(), this::readLines, (a, b) -> b)
        );
    }
}