package com.java.s4n.business.observable;

import com.java.s4n.business.loader.FileImporter;
import com.java.s4n.business.loader.Importable;
import com.java.s4n.business.mapper.ConveyorMapper;
import com.java.s4n.business.observer.Observer;
import com.java.s4n.domain.Position;
import com.java.s4n.domain.factory.Conveyor;
import com.java.s4n.domain.request.DeliveryRequestModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DeliveryStation implements Observable {

    private Importable importer;
    private ConveyorMapper conveyorMapper;
    private List<Observer> observers;
    private List<Conveyor> conveyors;

    public DeliveryStation(DeliveryRequestModel model) {
        this.importer = new FileImporter(model);
        this.conveyorMapper = new ConveyorMapper(model);
        this.observers = new ArrayList<>();
        this.conveyors = new ArrayList<>();
    }

    @Override
    public void register(Observer o) {
        observers.add(o);
    }

    @Override
    public void unregister(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers(String conveyorName, List<Position> deliveryResult) {
        observers.forEach(observer -> observer.update(conveyorName, deliveryResult));
    }

    public void deliverOrders() {
        loadConveyors();
        conveyors.forEach(conveyor -> {
            List<Position> deliveredList = conveyor.deliver();
            if (!deliveredList.isEmpty()) {
                this.notifyObservers(conveyor.getName(), deliveredList);
            }
        });
    }

    private void loadConveyors() {
        Map<String, List<String>> coordinatesByConveyorName = importer.importCoordinatesByConveyorName();
        this.conveyors.addAll(conveyorMapper.getConveyors(coordinatesByConveyorName));
    }
}
