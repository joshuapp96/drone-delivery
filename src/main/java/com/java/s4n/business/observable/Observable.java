package com.java.s4n.business.observable;

import com.java.s4n.business.observer.Observer;
import com.java.s4n.domain.Position;

import java.util.List;

public interface Observable {

    void register(Observer o);

    void unregister(Observer o);

    void notifyObservers(String conveyorName, List<Position> deliveryResult);
}
