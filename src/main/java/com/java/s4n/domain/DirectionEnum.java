package com.java.s4n.domain;

public enum DirectionEnum {

    NORTE("Norte") {
        @Override
        public DirectionEnum toLeft() {
            return OESTE;
        }

        @Override
        public DirectionEnum toRight() {
            return ESTE;
        }
    },
    SUR("Sur") {
        @Override
        public DirectionEnum toLeft() {
            return ESTE;
        }

        @Override
        public DirectionEnum toRight() {
            return OESTE;
        }
    },
    ESTE("Este") {
        @Override
        public DirectionEnum toLeft() {
            return NORTE;
        }

        @Override
        public DirectionEnum toRight() {
            return SUR;
        }
    },
    OESTE("Oeste") {
        @Override
        public DirectionEnum toLeft() {
            return SUR;
        }

        @Override
        public DirectionEnum toRight() {
            return NORTE;
        }
    };

    private final String directionName;

    DirectionEnum(String directionName) {
        this.directionName = directionName;
    }

    public abstract DirectionEnum toLeft();

    public abstract DirectionEnum toRight();

    public String getDirectionName() {
        return directionName;
    }
}
