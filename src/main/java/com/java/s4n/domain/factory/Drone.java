package com.java.s4n.domain.factory;

import com.java.s4n.business.strategy.DeliverBehavior;
import com.java.s4n.domain.Position;

import java.util.List;
import java.util.stream.Collectors;

public class Drone extends Conveyor {

    public static class Builder extends Conveyor.Builder {

        public Builder(DeliverBehavior deliverBehavior) {
            super(deliverBehavior);
        }
        
        @Override
        public Conveyor build() {
            return new Drone(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }

    private Drone(Builder builder) {
        super(builder);
    }

    @Override
    public List<Position> deliver() {
        return this.getRoutes()
                .stream()
                .map(route -> getDeliverBehavior().deliver(getDeliveryCoverage(), getInitialPosition(), route))
                .collect(Collectors.toList());
    }
    
}
