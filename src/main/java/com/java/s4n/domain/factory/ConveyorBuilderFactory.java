package com.java.s4n.domain.factory;

import com.java.s4n.business.strategy.DroneDeliver;
import com.java.s4n.domain.ConveyorTypeEnum;

import static com.java.s4n.domain.ConveyorTypeEnum.DRONE;

public class ConveyorBuilderFactory {

    public Conveyor.Builder createConveyorBuilder(ConveyorTypeEnum conveyorType) {
        if (conveyorType == DRONE) {
            return new Drone.Builder(new DroneDeliver());
        } else {
            System.out.println("No valid Conveyor type.");
        }
        return null;
    }
}