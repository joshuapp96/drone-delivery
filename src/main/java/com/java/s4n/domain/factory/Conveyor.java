package com.java.s4n.domain.factory;

import com.java.s4n.business.strategy.DeliverBehavior;
import com.java.s4n.domain.Position;
import com.java.s4n.domain.Route;

import java.util.List;

public abstract class Conveyor {

    private List<Route> routes;
    private String name;
    private Integer capacity;
    private Integer deliveryCoverage;
    private Position initialPosition;
    private DeliverBehavior deliverBehavior;

    public abstract static class Builder<T extends Builder<T>> {
        List<Route> routes;
        String name;
        Integer capacity;
        Integer deliveryCoverage;
        DeliverBehavior deliverBehavior;

        public Builder(DeliverBehavior deliverBehavior) {
            this.deliverBehavior = deliverBehavior;
        }

        public T withRoutes(List<Route> routes) {
            this.routes = routes;
            return self();
        }

        public T withName(String name) {
            this.name = name;
            return self();
        }

        public T withCapacity(Integer capacity) {
            this.capacity = capacity;
            return self();
        }

        public T withDeliveryCoverage(Integer deliveryCoverage) {
            this.deliveryCoverage = deliveryCoverage;
            return self();
        }

        public abstract Conveyor build();

        protected abstract T self();
    }

    public Conveyor(Builder<?> builder) {
        routes = builder.routes;
        name = builder.name;
        capacity = builder.capacity;
        deliveryCoverage = builder.deliveryCoverage;
        deliverBehavior = builder.deliverBehavior;
        initialPosition = Position.initial();
    }

    public abstract List<Position> deliver();

    public String getName() {
        return name;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public Integer getDeliveryCoverage() {
        return deliveryCoverage;
    }

    public DeliverBehavior getDeliverBehavior() {
        return deliverBehavior;
    }

    public Position getInitialPosition() {
        return initialPosition;
    }
}