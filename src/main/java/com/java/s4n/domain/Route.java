package com.java.s4n.domain;

public class Route {

    private final String coordinates;

    public Route(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getCoordinates() {
        return coordinates;
    }
}
