package com.java.s4n.domain.request;

import com.java.s4n.domain.ConveyorTypeEnum;

public class DeliveryRequestModel {

    public static class Builder {

        private ConveyorTypeEnum conveyorType = ConveyorTypeEnum.DRONE;
        private String inputFilePrefix = "in";
        private String outputFilePrefix = "out";
        private String fileExtension = ".txt";
        private String header = "== Reporte de entregas ==";
        private String inputPath = "src/main/resources/input/";
        private String outputPath = "src/main/resources/output/";
        private Integer conveyorCapacity = 3;
        private Integer conveyorFleetSize = 20;
        private Integer deliverCoverage = 10;

        public Builder withConveyorType(ConveyorTypeEnum conveyorType) {
            this.conveyorType = conveyorType;
            return this;
        }

        public Builder withInputFilePrefix(String inputFilePrefix) {
            this.inputFilePrefix = inputFilePrefix;
            return this;
        }

        public Builder withOutputFilePrefix(String outputFilePrefix) {
            this.outputFilePrefix = outputFilePrefix;
            return this;
        }

        public Builder withFileExtension(String fileExtension) {
            this.fileExtension = fileExtension;
            return this;
        }

        public Builder withHeader(String header) {
            this.header = header;
            return this;
        }

        public Builder withInputPath(String inputPath) {
            this.inputPath = inputPath;
            return this;
        }

        public Builder withOutputPath(String outputPath) {
            this.outputPath = outputPath;
            return this;
        }

        public Builder withConveyorCapacity(Integer conveyorCapacity) {
            this.conveyorCapacity = conveyorCapacity;
            return this;
        }

        public Builder withConveyorFleetSize(Integer conveyorFleetSize) {
            this.conveyorFleetSize = conveyorFleetSize;
            return this;
        }

        public Builder withDeliverCoverage(Integer deliverCoverage) {
            this.deliverCoverage = deliverCoverage;
            return this;
        }

        public DeliveryRequestModel build() {
            DeliveryRequestModel model = new DeliveryRequestModel();
            model.conveyorType = this.conveyorType;
            model.inputFilePrefix = this.inputFilePrefix;
            model.outputFilePrefix = this.outputFilePrefix;
            model.fileExtension = this.fileExtension;
            model.header = this.header;
            model.inputPath = this.inputPath;
            model.outputPath = this.outputPath;
            model.conveyorCapacity = this.conveyorCapacity;
            model.conveyorFleetSize = this.conveyorFleetSize;
            model.deliverCoverage = this.deliverCoverage;

            return model;
        }

    }

    private ConveyorTypeEnum conveyorType;
    private String inputFilePrefix;
    private String outputFilePrefix;
    private String fileExtension;
    private String header;
    private String inputPath;
    private String outputPath;
    private Integer conveyorCapacity;
    private Integer conveyorFleetSize;
    private Integer deliverCoverage;

    private DeliveryRequestModel() {

    }

    public ConveyorTypeEnum getConveyorType() {
        return conveyorType;
    }

    public String getHeader() {
        return header;
    }

    public String getInputFilePrefix() {
        return inputFilePrefix;
    }

    public String getOutputFilePrefix() {
        return outputFilePrefix;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public String getInputPath() {
        return inputPath;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public Integer getConveyorCapacity() {
        return conveyorCapacity;
    }

    public Integer getConveyorFleetSize() {
        return conveyorFleetSize;
    }

    public Integer getDeliverCoverage() {
        return deliverCoverage;
    }
}