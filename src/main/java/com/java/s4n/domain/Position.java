package com.java.s4n.domain;

public class Position {

    private int y;
    private int x;
    private DirectionEnum direction;

    public Position(int x, int y, DirectionEnum direction) {
        this.y = y;
        this.x = x;
        this.direction = direction;
    }

    public static Position from(Position position) {
        return new Position(position.x, position.y, position.direction);
    }

    public static Position initial() {
        return new Position(0, 0, DirectionEnum.NORTE);
    }

    public void incrementXAxis() {
        this.x++;
    }

    public void decrementXAxis() {
        this.x--;
    }

    public void incrementYAxis() {
        this.y++;
    }

    public void decrementYAxis() {
        this.y--;
    }

    public DirectionEnum getDirection() {
        return direction;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void setDirection(DirectionEnum direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return String.format("(%d, %d) dirección %s", x, y, direction.getDirectionName());
    }
}
